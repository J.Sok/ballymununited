var map;
function initMap() {
  const loc = { lat: 53.406958, lng: -6.278571 };
  const map = new google.maps.Map(document.getElementById("map"), {
    center: loc,
    zoom: 16,
  });

  const marker = new google.maps.Marker({ position: loc, map: map });
}
